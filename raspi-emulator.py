#!/usr/bin/env python3

import argparse
import contextlib
import hashlib
import math
import os
import pathlib
import shlex
import shutil
import subprocess
import tempfile
import typing


Raspi = typing.NamedTuple(
    "Raspi",
    [
        ("machine", str),
        ("dtb", str),
        ("cpu", str),
        ("cores", int),
        ("ram", str),
    ],
)
RASPI_VERSIONS: dict[str, Raspi] = {
    "3B": Raspi(
        machine="raspi3b",
        dtb="bcm2837-rpi-3-b.dtb",
        cpu="cortex-a53",
        cores=4,
        ram="1G",
    ),
    "3A+": Raspi(
        machine="raspi3ap",
        dtb="bcm2837-rpi-3-a-plus.dtb",
        cpu="cortex-a53",
        cores=4,
        ram="512M",
    ),
    "3B+": Raspi(
        machine="raspi3bp",
        dtb="bcm2837-rpi-3-b-plus.dtb",
        cpu="cortex-a53",
        cores=4,
        ram="1G",
    ),
    "4B": Raspi(
        machine="raspi4b",
        dtb="bcm2711-rpi-4-b.dtb",
        cpu="cortex-a72",
        cores=4,
        ram="1G",
    ),
}


def _run(command: str, **kwargs) -> subprocess.CompletedProcess:
    print(f"\x1b[1m{command}\x1b[0m")
    return subprocess.run(
        shlex.split(command),
        **kwargs,
    )


def hash_image(image_path: pathlib.Path) -> str:
    BLOCKSIZE = 65536
    hasher = hashlib.sha256()
    with image_path.open("rb") as fp:
        while data := fp.read(BLOCKSIZE):
            hasher.update(data)
    return hasher.hexdigest()


def extract_disk(
    *,
    image_path: pathlib.Path,
    destination=pathlib.Path,
) -> tuple[str, pathlib.Path]:
    image_hash = hash_image(image_path)
    disk_path = destination / f"{image_hash}.disk"

    if disk_path.exists():
        return image_hash, disk_path

    destination.mkdir(parents=True, exist_ok=True)
    if image_path.suffix == ".xz":
        with disk_path.open("wb") as fp:
            _run(
                f"xz --decompress --keep --threads=0 --verbose --stdout {image_path}",
                stdout=fp,
                text=False,
            )
    else:
        shutil.copy(image_path, disk_path)

    fix_disk_size(disk_path)
    return image_hash, disk_path


@contextlib.contextmanager
def losetup(path: pathlib.Path) -> typing.Generator[pathlib.Path, None, None]:
    process = _run(
        f"sudo losetup --find --show --partscan {path}",
        text=True,
        stdout=subprocess.PIPE,
        check=True,
    )
    loop_device = process.stdout.strip()
    if not loop_device:
        raise RuntimeError(f"losetup: Unable to mount image {path}")

    try:
        yield pathlib.Path(loop_device)
    finally:
        _run(f"sudo losetup --detach {loop_device}")


@contextlib.contextmanager
def mount(path: pathlib.Path) -> typing.Generator[pathlib.Path, None, None]:
    with tempfile.TemporaryDirectory() as directory:
        _run(f"sudo mount -o uid={os.getuid()} {path} {directory}", check=True)
        try:
            yield pathlib.Path(directory)
        finally:
            _run(f"sudo umount {directory}")


def extract_kernel_name(config_txt: pathlib.Path) -> str:
    content = config_txt.read_text()
    for line in content.splitlines():
        if line.startswith("kernel="):
            return line.split("=")[1]
    raise RuntimeError("Unable to detect kernel image")


def extract_dtb_and_kernel(
    *,
    disk_path: pathlib.Path,
    image_hash: str,
    raspi_version: str,
    destination=pathlib.Path,
) -> tuple[pathlib.Path, pathlib.Path, str, pathlib.Path]:
    destination.mkdir(parents=True, exist_ok=True)
    dtb_path = destination / f"{image_hash}-{raspi_version}.dtb"
    kernel_path = destination / f"{image_hash}.kernel"
    kernel_args_path = destination / f"{image_hash}.kernel_args"
    initrd_path = destination / f"{image_hash}.initrd"

    if (
        dtb_path.exists()
        and kernel_path.exists()
        and kernel_args_path.exists()
        and initrd_path.exists()
    ):
        return dtb_path, kernel_path, kernel_args_path.read_text(), initrd_path

    with losetup(disk_path) as device:
        partition = device.with_name(f"{device.name}p1")
        with mount(partition) as directory:
            # dtb
            dtb_filename = RASPI_VERSIONS[raspi_version].dtb
            shutil.copy(directory / dtb_filename, dtb_path)

            # kernel
            kernel_filename = extract_kernel_name(directory / "config.txt")
            shutil.copy(directory / kernel_filename, kernel_path)

            # kernel_args
            cmdline_txt = directory / "cmdline.txt"
            kernel_args = cmdline_txt.read_text().strip()
            kernel_args_path.write_text(kernel_args)

            # initrd
            initrd_filename = kernel_filename.replace("vmlinuz", "initrd.img")
            shutil.copy(directory / initrd_filename, initrd_path)

    return dtb_path, kernel_path, kernel_args, initrd_path


def fix_disk_size(image: pathlib.Path) -> None:
    current_size = image.stat().st_size / 1024 ** 3
    size = 2 ** min(1, math.ceil(math.log2(current_size)))
    _run(f"qemu-img resize -f raw {image} {size}G")


def launch(
    *,
    raspi_version: str,
    dtb: pathlib.Path,
    kernel: pathlib.Path,
    kernel_args: str,
    initrd: pathlib.Path,
    disk: pathlib.Path,
    ssh_port: int,
    use_virt: bool,
) -> None:
    raspi = RASPI_VERSIONS[raspi_version]
    kernel_args = "rw root=/dev/vda2 net.ifnames=0" if use_virt else kernel_args
    args = {
        "machine": "virt" if use_virt else raspi.machine,
        "cpu": raspi.cpu,
        "m": raspi.ram,
        "smp": str(raspi.cores),
        "dtb": False if use_virt else str(dtb),
        "kernel": str(kernel),
        "append": f'"{kernel_args}"',
        "initrd": str(initrd),
        "drive": f"file={disk},index=0,media=disk,format=raw",
        "serial": "mon:stdio",
        "-nographic": True,
        "nic": f"user,model=e1000,hostfwd=tcp::{ssh_port}-:22",
    }
    command_args = " ".join(
        f"-{key} {'' if value is True else value}"
        for key, value in args.items()
        if value
    )
    _run(f"qemu-system-aarch64 {command_args}")


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument("--pi", default="3B", choices=RASPI_VERSIONS)
    parser.add_argument("--use-virt", action="store_true")
    parser.add_argument("--ssh-port", default=10022, type=int)
    parser.add_argument(
        "--build-dir",
        default=pathlib.Path("./build"),
        type=pathlib.Path,
    )
    parser.add_argument("image", type=pathlib.Path)

    args = parser.parse_args()
    image_hash, disk_path = extract_disk(
        image_path=args.image,
        destination=args.build_dir,
    )
    dtb, kernel, kernel_args, initrd = extract_dtb_and_kernel(
        disk_path=disk_path,
        image_hash=image_hash,
        raspi_version=args.pi,
        destination=args.build_dir,
    )
    launch(
        raspi_version=args.pi,
        dtb=dtb,
        kernel=kernel,
        kernel_args=kernel_args,
        initrd=initrd,
        disk=disk_path,
        use_virt=args.use_virt,
        ssh_port=args.ssh_port,
    )


if __name__ == "__main__":
    main()
