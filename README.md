# raspi-emulator, Simple Raspberry PI emulator

This projet is for now a simple qemu wrapper that allow to run Debian or other
distribution image on a virtual Raspberry PI.

## Requirements

To use _raspi-emulator_, the following commands should be available:

- `xz`: to decompress image
- `sudo`: to gain root privileges (needed to extract kernel and dtd) **:warning:**
- `losetup`: to extract kernel and dtd
- `qemu-img`: to resize disk image
- `qemu-system-aarch64`: to launch the virtual Raspberry PI
